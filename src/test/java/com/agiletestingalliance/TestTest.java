package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestTest {

    @Test
    public void testGstr() {
        // Given
        String inputString = "Hello, World!";
        com.agiletestingalliance.Test testInstance = new com.agiletestingalliance.Test(inputString);

        // When
        String result = testInstance.gstr();

        // Then
        assertEquals(inputString, result);
    }
}