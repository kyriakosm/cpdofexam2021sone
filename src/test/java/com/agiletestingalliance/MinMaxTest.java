package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void testF() {
        // Given
        MinMax minMax = new MinMax();

        // When
        int result = minMax.f(5, 10);

        // Then
        assertEquals(10, result);
    }

    @Test
    public void testBarNotEmpty() {
        // Given
        MinMax minMax = new MinMax();

        // When
        String result = minMax.bar("hello");

        // Then
        assertEquals("HELLO", result);
    }

    @Test
    public void testBarEmpty() {
        // Given
        MinMax minMax = new MinMax();

        // When
        String result = minMax.bar("");

        // Then
        assertEquals("Input should not be empty", result);
    }

    @Test
    public void testBarNull() {
        // Given
        MinMax minMax = new MinMax();

        // When
        String result = minMax.bar(null);

        // Then
        assertEquals("Input should not be empty", result);
    }
}