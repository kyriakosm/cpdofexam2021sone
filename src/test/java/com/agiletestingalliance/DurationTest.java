package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DurationTest {
    @Test
    public void testDur() {
        Duration duration = new Duration();
        String expectedDescription = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        String actualDescription = duration.dur();
        assertEquals(expectedDescription, actualDescription);
    }

    @Test
    public void testCalculateIntValueDoesNothing() {
        Duration duration = new Duration();
        // This test verifies that calling 'calculateIntValue' doesn't affect the state of the object
        duration.calculateIntValue();
        // We don't have specific expectations regarding the outcome of this method because it doesn't change anything observable
    }
}
