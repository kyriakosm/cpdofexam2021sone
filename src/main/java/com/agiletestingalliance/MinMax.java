package com.agiletestingalliance;

public class MinMax {

	public int f(int a, int b) {
		if (b > a)
			return b;
		else
			return a; 
	}

	
  public String bar(String arg){
    if (arg == null || arg.isEmpty()) {
        return "Input should not be empty";
    } else {
        return arg.toUpperCase();
    }
  }
	

}
